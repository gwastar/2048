CFLAGS += -O3 -march=native -mtune=native -Wall -fPIE
# CFLAGS += -O0 -g -Wall -fPIE
CFLAGS += -Wno-unused-function
CFLAGS += $(shell pkg-config --cflags ncurses)
LFLAGS += $(shell pkg-config --libs ncurses) -lm -pthread

VERBOSE ?= @

BINARY = 2048

OBJDIR = build
DEPDIR = dep

SOURCES = $(wildcard src/*.c)
DEP_FILES = $(patsubst %.c,$(DEPDIR)/%.d,$(SOURCES))
OBJECTS = $(patsubst %.c,$(OBJDIR)/%.o,$(SOURCES))

.PHONY = all clean

all: $(BINARY) $(MAKEFILE_LIST)

$(OBJDIR)/%.o: %.c $(MAKEFILE_LIST)
	$(VERBOSE) mkdir -p $(@D)
	$(VERBOSE) $(CC) $(CFLAGS) -c $(filter %.c,$^) -o $@

$(DEPDIR)/%.d: %.c $(MAKEFILE_LIST)
	$(VERBOSE) mkdir -p $(@D)
	$(VERBOSE) $(CC) $(CFLAGS) -MM -MT $(OBJDIR)/$*.o -MF $@ $<

$(BINARY): $(OBJECTS) $(MAKEFILE_LIST)
	$(VERBOSE) mkdir -p $(@D)
	$(VERBOSE) $(CC) $(filter %.o,$^) -o $@ $(LFLAGS)

clean: $(MAKEFILE_LIST)
	$(VERBOSE) rm -rf $(OBJDIR)
	$(VERBOSE) rm -rf $(DEPDIR)
	$(VERBOSE) rm -rf $(BINARY)

ifneq ($(MAKECMDGOALS),clean)
-include $(DEP_FILES)
endif
