#include <pthread.h>
#include <threads.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/random.h>
#include <ncurses.h>

#include "random.h"

#define MONTE_CARLO

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define BOARDSIZE 4

typedef struct {
	uint8_t tiles[BOARDSIZE][BOARDSIZE];
} board_t;

struct tile_index {
#if BOARDSIZE < 256
	uint8_t j;
	uint8_t i;
#else
	uint16_t j;
	uint16_t i;
#endif
};

static thread_local struct random_state global_random_state;

static float random_uniform_float(void)
{
	return random_next_uniform_float(&global_random_state);
}

static uint32_t random_u32(void)
{
	return random_next_u32(&global_random_state);
}

static bool random_bool(void)
{
	return random_next_bool(&global_random_state);
}

static unsigned int get_empty_tiles(struct tile_index *empty_tiles, const board_t *board)
{
	unsigned int n = 0;
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			if (board->tiles[j][i] == 0) {
				empty_tiles[n].j = j;
				empty_tiles[n].i = i;
				n++;
			}
		}
	}
	return n;
}

static void spawn_random_number(board_t *board)
{
	struct tile_index empty_tiles[BOARDSIZE * BOARDSIZE];
	unsigned int num_empty_tiles = get_empty_tiles(empty_tiles, board);
	struct tile_index idx = empty_tiles[random_u32() % num_empty_tiles];
	board->tiles[idx.j][idx.i] = random_uniform_float() < 0.9f ? 1 : 2;
}

static bool is_lost(const board_t *board)
{
	// checking for empty cells
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			if (board->tiles[j][i] == 0) {
				return false;
			}
		}
	}
	// no empty cells from here

	// checking for same values horizontally
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		for (unsigned int i = 0; i < BOARDSIZE - 1; i++) {
			if (board->tiles[j][i] == board->tiles[j][i + 1]) {
				return false;
			}
		}
	}
	// checking for same values vertically
	for (unsigned int j = 0; j < BOARDSIZE - 1; j++) {
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			if (board->tiles[j][i] == board->tiles[j + 1][i]) {
				return false;
			}
		}
	}
	return true;
}

enum direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,

	DIR_COUNT,
	DIR_FIRST = UP,
	DIR_LAST = RIGHT,
};

static uint8_t *get_tile(unsigned int j, unsigned int i, enum direction dir, board_t *board)
{
	switch(dir) {
	case LEFT:
		return &board->tiles[j][i];
	case RIGHT:
		return &board->tiles[j][BOARDSIZE - 1 - i];
	case UP:
		return &board->tiles[i][j];
	case DOWN:
		return &board->tiles[BOARDSIZE - 1 - i][j];
	default:
		return NULL;
	}
}

struct board_move_data {
	unsigned int score;
	unsigned int linear_score;
	unsigned int merges;
	bool did_change;
};

static struct board_move_data move_board(board_t *board, enum direction dir)
{
	bool did_change = false;
	unsigned int score = 0;
	unsigned int linear_score = 0;
	unsigned int merges = 0;
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		uint8_t last_number = 0;
		uint8_t numbers[BOARDSIZE] = {0};
		unsigned int n = 0;
		// TODO try doing this in one loop and iterating in the other direction
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			uint8_t *tile = get_tile(j, i, dir, board);
			if (*tile != 0) {
				if (*tile == last_number) {
					numbers[n - 1]++;
					score += (1lu << last_number);
					linear_score += last_number;
					merges++;
					did_change |= true;
				} else {
					did_change |= n != i;
					numbers[n++] = *tile;
				}
				last_number = numbers[n - 1];
				*tile = 0;
			}
		}
		for (unsigned int i = 0; i < n; i++) {
			uint8_t *tile = get_tile(j, i, dir, board);
			*tile = numbers[i];
		}
	}
	return (struct board_move_data) {
		.score = score,
		.linear_score = linear_score,
		.merges = merges,
		.did_change = did_change
	};
}

#define LOST_SCORE (-10000.0f)

static double normalize_tile_score(unsigned int score)
{
	return (1.0 / (1 << 16)) * score;
}

static double normalize_linear_tile_score(unsigned int score)
{
	return (1.0 / 16) * score;
}

static double get_merge_score(double merges)
{
	return merges / (BOARDSIZE * BOARDSIZE / 2);
}

static float get_gradient_score(const board_t *board)
{
	board_t compact = {0};
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		unsigned int n = 0;
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			uint8_t tile = board->tiles[j][i];
			if (tile != 0) {
				compact.tiles[j][n++] = tile;
			}
		}
	}
	for (unsigned int i = 0; i < BOARDSIZE; i++) {
		unsigned int n = 0;
		for (unsigned int j = 0; j < BOARDSIZE; j++) {
			uint8_t tile = compact.tiles[j][i];
			if (tile != 0) {
				compact.tiles[n++][i] = tile;
			}
		}
	}
	float g = 0;
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		for (unsigned int i = 0; i < BOARDSIZE - 1; i++) {
			uint8_t tile = compact.tiles[j][i];
			if (tile == 0) {
				break;
			}
			uint8_t next = compact.tiles[j][i + 1];
			float a = normalize_linear_tile_score(tile);
			float b = normalize_linear_tile_score(next);
			float d = -(1.0f / BOARDSIZE) * fabsf(a - b) * (a < b ? 2.0f : 0.5f);
			g += d;
		}
	}
	return g;
}

static float evaluate_board(const board_t *board)
{
	uint8_t highest = 0;
	unsigned int zeros = 0;
	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			uint8_t val = board->tiles[j][i];
			if (val == 0) {
				zeros++;
			}
			if (val > highest) {
				highest = val;
			}
		}
	}
	float score_highest = normalize_linear_tile_score(highest);
	float score_zeros = (float)zeros / (BOARDSIZE * BOARDSIZE);
	// float score_gradient = get_gradient_score(board);
	float score_gradient = 0;
	return score_highest + 0.2f * score_zeros + 0.2f * score_gradient;
 }

#ifndef MONTE_CARLO

static float evaluate_directions(board_t board, unsigned int depth);

static float evaluate_random_spawns(board_t board, unsigned int depth)
{
	struct tile_index empty_tiles[BOARDSIZE * BOARDSIZE];
	unsigned int num_empty_tiles = get_empty_tiles(empty_tiles, &board);
#if 0
	// minimax
	double worst_score = INFINITY;
	for (unsigned int i = 0; i < num_empty_tiles; i++) {
		struct tile_index idx = empty_tiles[i];

		board.tiles[idx.j][idx.i] = 1;
		float score = evaluate_directions(board, depth);
		if (score < worst_score) {
			worst_score = score;
		}

		board.tiles[idx.j][idx.i] = 2;
		score = evaluate_directions(board, depth);
		if (score < worst_score) {
			worst_score = score;
		}

		board.tiles[idx.j][idx.i] = 0;
	}
	return worst_score;
#else
	// expectimax
	double total = 0;
	for (unsigned int i = 0; i < num_empty_tiles; i++) {
		struct tile_index idx = empty_tiles[i];

		board.tiles[idx.j][idx.i] = 1;
		float score = evaluate_directions(board, depth);
		total += 0.9f * score;

		board.tiles[idx.j][idx.i] = 2;
		score = evaluate_directions(board, depth);
		total += 0.1f * score;

		board.tiles[idx.j][idx.i] = 0;
	}
	return total / num_empty_tiles;
#endif
}

static float evaluate_directions(board_t board, unsigned int depth)
{
	if (depth == 0) {
		return evaluate_board(&board) + (is_lost(&board) ? LOST_SCORE : 0);
	}

	float best_score = LOST_SCORE;
	for (enum direction dir = DIR_FIRST; dir <= DIR_LAST; dir++) {
		board_t new_board = board;
		struct board_move_data d = move_board(&board, dir);
		if (!d.did_change) {
			continue;
		}
		float score = evaluate_random_spawns(new_board, depth - 1);
		score += d.linear_score;
		score += get_merge_score(d.merges);
		if (score > best_score) {
			best_score = score;
		}
	}
	return best_score;
}

#else

#define MONTE_CARLO_MAX_DEPTH 50
#define MONTE_CARLO_ITERATIONS 200000

static float evaluate_monte_carlo(board_t initial_board)
{
	double total = 0;
	unsigned int runs = 0;
	for (unsigned int i = 0; i < MONTE_CARLO_ITERATIONS; runs++) {
		board_t board = initial_board;
		bool lost = false;
		unsigned int score = 0;
		unsigned int merges = 0;
		unsigned int depth;
		for (depth = 0;
		     depth < MONTE_CARLO_MAX_DEPTH && i < MONTE_CARLO_ITERATIONS && !lost;
		     depth++, i++) {
			spawn_random_number(&board);
			// TODO make a version that follows all directions like "evaluate_directions"
#if 0
			// follow "best" direction (seems worse)
			unsigned int best_score = 0;
			unsigned int best_merges = 0;
			bool did_change = false;
			for (enum direction dir = DIR_FIRST; dir <= DIR_LAST; dir++) {
				board_t copy = board;
				struct board_move_data d = move_board(&copy, dir);
				if (d.did_change) {
					did_change = true;
					unsigned int s = d.linear_score;
					unsigned int m = d.merges;
					if (s > best_score || (s == best_score && random_bool())) {
					// if (m > best_merges || (m == best_merges && random_bool())) {
						best_score = s;
						best_merges = m;
						board = copy;
					}
				}
			}
			i += DIR_COUNT / 2; // account for the extra work compared to the other version below
			score += best_score;
			merges += best_merges;
#else
			// follow random direction
			unsigned int start_dir = random_u32() % DIR_COUNT;
			enum direction dir = start_dir;
			bool did_change = false;
			do {
				struct board_move_data d = move_board(&board, dir);
				if (d.did_change) {
					score += d.linear_score;
					merges += d.merges;
					did_change = true;
					break;
				}
				dir = (dir + 1) % (DIR_COUNT);
			} while (dir != start_dir);
#endif
			if (!did_change) {
				lost = true;
			}
		}

		const double inv_depth = 1.0 / depth;
		total += 0.5 * normalize_linear_tile_score(inv_depth * score);
		total += 0.5 * get_merge_score(inv_depth * merges);
		total += 1.0 * evaluate_board(&board);
		if (lost) {
			total += LOST_SCORE;
		}
	}
	return total / runs;
}

#endif

#ifndef MONTE_CARLO
# define THREAD_COUNT DIR_COUNT
#else
# define THREAD_COUNT (2 * DIR_COUNT)
#endif

static struct random_state eval_thread_random_states[THREAD_COUNT];
static board_t eval_boards[DIR_COUNT];
static float eval_scores[THREAD_COUNT];
static pthread_barrier_t barrier;

static void *eval_thread_func(void *arg)
{
	uintptr_t tidx = (uintptr_t)arg;
	enum direction dir = (enum direction)(tidx % DIR_COUNT);
	global_random_state = eval_thread_random_states[dir];
	for (;;) {
		pthread_barrier_wait(&barrier);
		board_t board = eval_boards[dir];
		eval_scores[tidx] = -INFINITY;
		if (move_board(&board, dir).did_change) {
#ifndef MONTE_CARLO
			const unsigned int MAX_DEPTH = 3;
			eval_scores[tidx] = evaluate_random_spawns(board, MAX_DEPTH);
#else
			eval_scores[tidx] = evaluate_monte_carlo(board);
#endif
		}
		pthread_barrier_wait(&barrier);
	}
	return NULL;
}

static enum direction find_best_move(const board_t *board)
{
	for (enum direction dir = DIR_FIRST; dir <= DIR_LAST; dir++) {
		eval_boards[dir] = *board;
	}
	pthread_barrier_wait(&barrier);
	pthread_barrier_wait(&barrier);

	double scores[DIR_COUNT] = {0};
	unsigned int counts[DIR_COUNT] = {0};
	for (uintptr_t i = 0; i < THREAD_COUNT; i++) {
		scores[i % DIR_COUNT] += eval_scores[i];
		counts[i % DIR_COUNT] += 1;
	}

	float best_score = -INFINITY;
	enum direction best_dir = 0;
	for (enum direction dir = DIR_FIRST; dir <= DIR_LAST; dir++) {
		float score = scores[dir] / counts[dir];
		if (score > best_score) {
			best_score = score;
			best_dir = dir;
		}
	}
	return best_dir;
}

struct solver_thread_data {
	board_t board;
	unsigned long *score;
	pthread_mutex_t lock;
	pthread_cond_t cond;
	struct random_state random_state;
};

#define UPDATE_INTERVAL 16

static void *solver_thread_func(void *arg)
{
	struct solver_thread_data *data = arg;
	global_random_state = data->random_state;
	board_t *board = &data->board;
	bool lost = false;
	pthread_mutex_lock(&data->lock);
	for (size_t i = 0; !lost; i++) {
		if (i % UPDATE_INTERVAL == 0) {
			pthread_cond_signal(&data->cond);
			pthread_mutex_unlock(&data->lock);
		}
		enum direction dir = find_best_move(board);
		if (i % UPDATE_INTERVAL == 0) {
			pthread_mutex_lock(&data->lock);
		}
		struct board_move_data d = move_board(board, dir);
		lost = !d.did_change && is_lost(board);
		if (d.did_change) {
			(*data->score) += d.score;
			spawn_random_number(board);
		}
	}
	pthread_cond_signal(&data->cond);
	pthread_mutex_unlock(&data->lock);
	return NULL;
}

static const uint32_t tile_background_colors[] = {
	0xcdc1b4,
	0xeee4da,
	0xede0c8,
	0xf2b179,
	0xf59563,
	0xf67c5f,
	0xf65e3b,
	0xedcf72,
	0xedcc61,
	0xedc850,
	0xedc53f,
	0xedc22e,
	0x3c3a32,
};
static const uint32_t tile_foreground_color1 = 0x776e65;
static const uint32_t tile_foreground_color2 = 0xf9f6f2;
static const uint32_t foreground_color = 0x4b3d30;
static const uint32_t background_color = 0xbbada0;
static const int num_colors = ARRAY_SIZE(tile_background_colors) + 4;
static const int num_tile_color_pairs = ARRAY_SIZE(tile_background_colors);
static const int num_color_pairs = num_tile_color_pairs + 1;

static void draw_board(const board_t *board, unsigned long score, bool color)
{
	erase();
	bkgd(COLOR_PAIR(1) | ' ');
	attron(A_BOLD);
	printw("Score: %lu", score);
	attroff(A_BOLD);
	refresh();

	int width, height;
	getmaxyx(stdscr, height, width);
	width -= 2;
	height -= 3;

	int tile_width = width / BOARDSIZE;
	int tile_height = height / BOARDSIZE;

	int yoffset = 2;
	int xoffset = 1;

	WINDOW *outer_win = newwin(tile_height * BOARDSIZE + 2,
				   tile_width * BOARDSIZE + 2,
				   yoffset - 1, xoffset - 1);
	if (color) {
		wattron(outer_win, COLOR_PAIR(1));
	}
	box(outer_win, 0, 0);
	wrefresh(outer_win);
	delwin(outer_win);

	for (unsigned int j = 0; j < BOARDSIZE; j++) {
		for (unsigned int i = 0; i < BOARDSIZE; i++) {
			int y = j * tile_height + yoffset;
			int x = i * tile_width + xoffset;
			unsigned long val = board->tiles[j][i];
			WINDOW *win = newwin(tile_height, tile_width, y, x);
			if (color) {
				int c = val;
				if (val >= num_tile_color_pairs) {
					c = num_tile_color_pairs - 1;
				}
				int color_pair = COLOR_PAIR(c + 2);
				wattron(win, color_pair);
				wbkgd(win, color_pair | ' ');
			}
			box(win, 0, 0);

			if (val) {
				wattron(win, A_BOLD);
				val = 1 << val;
				char number[32];
				int n = snprintf(number, sizeof(number), "%lu", val);
				wmove(win, tile_height / 2, (tile_width - n) / 2);
				waddstr(win, number);
				wattroff(win, A_BOLD);
			}

			wrefresh(win);
			delwin(win);
		}
	}
}

static void init_color_rgb(int color, uint32_t rgb)
{
	unsigned int r = (rgb >> 16) & 0xff;
	unsigned int g = (rgb >>  8) & 0xff;
	unsigned int b = (rgb >>  0) & 0xff;
	init_extended_color(color, r, g, b);
}

static bool init_colors(void)
{
	start_color();
	if (COLORS - 1 < num_colors || COLOR_PAIRS - 1 < num_color_pairs) {
		return false;
	}

	init_color_rgb(1, foreground_color);
	init_color_rgb(2, background_color);
	init_color_rgb(3, tile_foreground_color1);
	init_color_rgb(4, tile_foreground_color2);

	init_extended_pair(1, 1, 2);
	for (int i = 0; i < ARRAY_SIZE(tile_background_colors); i++) {
		int background = i + 5;
		init_color_rgb(background, tile_background_colors[i]);
		int foreground = i < 3 ? 3 : 4;
		init_extended_pair(i + 2, foreground, background);
	}

	return true;
}

int main(int argc, char **argv)
{
	if (argc > 1) {
		random_state_init(&global_random_state, atoi(argv[1]));
	} else {
		getrandom(&global_random_state, sizeof(global_random_state), 0);
	}

	pthread_barrier_init(&barrier, NULL, THREAD_COUNT + 1);
	for (uintptr_t i = 0; i < THREAD_COUNT; i++) {
		eval_thread_random_states[i] = global_random_state;
		random_jump(&global_random_state);
		pthread_t t;
		pthread_create(&t, NULL, eval_thread_func, (void *)i);
	}

	initscr();
	use_default_colors();
	cbreak();
	keypad(stdscr, true);
	curs_set(0);
	noecho();

	bool color = has_colors() && can_change_color();
	if (color) {
		color = init_colors();
	}

	board_t board = {0};
	unsigned long score = 0;
	spawn_random_number(&board);
	spawn_random_number(&board);

	bool automatic = true;
	if (automatic) {
		struct solver_thread_data data = {
			.board = board,
			.random_state = global_random_state,
			.score = &score,
		};
		random_jump(&global_random_state);
		pthread_mutex_init(&data.lock, false);
		pthread_cond_init(&data.cond, NULL);
		pthread_t thread;
		pthread_create(&thread, NULL, solver_thread_func, &data);
		for (;;) {
			pthread_mutex_lock(&data.lock);
			pthread_cond_wait(&data.cond, &data.lock);
			board = data.board;
			pthread_mutex_unlock(&data.lock);
			draw_board(&board, score, color);
			if (is_lost(&board)) {
				break;
			}
		}
		flushinp();
		getch();
		endwin();
		return 0;
	}

	bool quit = false;
	for (size_t i = 0; !quit; i++) {
		draw_board(&board, score, color);

		int ch = getch();
		if (ch == ERR) {
			break;
		}
		struct board_move_data d = {0};
		switch (ch) {
		case '\n':
			d = move_board(&board, find_best_move(&board));
			break;
		case KEY_UP:
		case 'w':
			d = move_board(&board, UP);
			break;
		case KEY_DOWN:
		case 's':
			d = move_board(&board, DOWN);
			break;
		case KEY_LEFT:
		case 'a':
			d = move_board(&board, LEFT);
			break;
		case KEY_RIGHT:
		case 'd':
			d = move_board(&board, RIGHT);
			break;
		case ERR:
		case 'q':
			quit = true;
			break;
		}

		if (d.did_change) {
			score += d.score;
			spawn_random_number(&board);
			if (is_lost(&board)) {
				break;
			}
		}
	}

	flushinp();
	getch();
	endwin();
}
